## ABA English Challenge Android

This project is a challenge for Android position at ABA English.

## About

The application using a external API (https://developers.themoviedb.org) that retrieve all informations like the popular TV shows, get the information of specific TV show and similar.

## Project details

#### API
The Movie Database API. (Docs): [https://developers.themoviedb.org/4/getting-started]

#### Architecture and Libraries

* Architecture MVVM
* API minimum 16 and target 29
* Client REST/HTTP with Retrofit + OkHttp
* Loading images with Picasso
* View binding using Databindg
* Unit tests with JUnit + Mockito

#### Additional

* Catch error cases or something like that (no connection network, server errors) ✔︎
* Parallax effect on Appbar ✔︎
* Unit tests ✔︎

```
## Licença

    The MIT License (MIT)

    Copyright (c) 2019 Paulo Camilo

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.