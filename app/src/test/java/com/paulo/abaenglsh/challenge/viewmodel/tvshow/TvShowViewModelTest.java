package com.paulo.abaenglsh.challenge.viewmodel.tvshow;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;

import com.jraska.livedata.TestObserver;
import com.paulo.abaenglsh.challenge.model.TvShowModel;
import com.paulo.abaenglsh.challenge.repository.TVShowRepository;
import com.paulo.abaenglsh.challenge.service.models.BaseData;
import com.paulo.abaenglsh.challenge.service.models.BaseResponse;
import com.paulo.abaenglsh.challenge.service.models.TvShow;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * The tests for TvShowViewModel
 *
 * Created by pcamilo on 30/07/2019
 */
@RunWith(JUnit4.class)
public class TvShowViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Mock
    TVShowRepository tvShowRepository;

    private TvShowViewModel viewModel;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        viewModel = new TvShowViewModel(tvShowRepository);
    }

    @Test
    public void getPopularTvShows() {
        // Given
        List<TvShow> tvShowList = new ArrayList<>();
        tvShowList.add(new TvShow());

        MutableLiveData<BaseResponse<List<TvShow>>> data = new MutableLiveData<>();
        data.setValue(new BaseResponse<>(tvShowList, true));

        // When
        when(tvShowRepository.getPopularTvShows(1, Locale.getDefault().toLanguageTag())).thenReturn(data);
        BaseData<List<TvShowModel>> value = TestObserver.test(viewModel.getPopularTvShows()).value();

        // Then
        assertNotNull(value.getData());
    }

    @Test
    public void getPopularTvShowsNull() {
        // Given
        MutableLiveData<BaseResponse<List<TvShow>>> data = new MutableLiveData<>();
        data.setValue(new BaseResponse<>(null, false));

        // When
        when(tvShowRepository.getPopularTvShows(1, Locale.getDefault().toLanguageTag())).thenReturn(data);
        BaseData<List<TvShowModel>> value = TestObserver.test(viewModel.getPopularTvShows()).value();

        // Then
        assertEquals(0, value.getData().size());
    }

    @Test
    public void getTvShowDetailsSuccess() {
        // Given
        TvShow tvShow = new TvShow();
        MutableLiveData<BaseResponse<TvShow>> data = new MutableLiveData<>();
        data.setValue(new BaseResponse<>(tvShow, true));

        // When
        when(tvShowRepository.getTvShowDetails(anyInt())).thenReturn(data);
        BaseData<TvShowModel> value = TestObserver.test(viewModel.getTvShowDetails(anyInt())).value();

        // Then
        assertNotNull(value.getData());
    }

    @Test
    public void getTvShowDetailsNull() {
        // Given
        MutableLiveData<BaseResponse<TvShow>> data = new MutableLiveData<>();
        data.setValue(new BaseResponse<>(null, false));

        // When
        when(tvShowRepository.getTvShowDetails(anyInt())).thenReturn(data);
        BaseData<TvShowModel> value = TestObserver.test(viewModel.getTvShowDetails(anyInt())).value();

        // Then
        assertNull(value.getData());
    }

    @Test
    public void getSimilarTvShows() {
        // Given
        List<TvShow> tvShowList = new ArrayList<>();
        tvShowList.add(new TvShow());

        MutableLiveData<BaseResponse<List<TvShow>>> data = new MutableLiveData<>();
        data.setValue(new BaseResponse<>(tvShowList, true));

        // When
        when(tvShowRepository.getTvShowsSimilar(1)).thenReturn(data);
        BaseData<List<TvShowModel>> value = TestObserver.test(viewModel.getSimilarTvShows(1)).value();

        // Then
        assertNotNull(value.getData());
    }

    @Test
    public void getSimilarTvShowsNull() {
        // Given
        MutableLiveData<BaseResponse<List<TvShow>>> data = new MutableLiveData<>();
        data.setValue(new BaseResponse<>(null, false));

        // When
        when(tvShowRepository.getTvShowsSimilar(1)).thenReturn(data);
        BaseData<List<TvShowModel>> value = TestObserver.test(viewModel.getSimilarTvShows(1)).value();

        // Then
        assertEquals(0, value.getData().size());
    }
}