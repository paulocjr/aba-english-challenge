package com.paulo.abaenglsh.challenge.di.modules;

import com.paulo.abaenglsh.challenge.di.app.AppComponent;
import com.paulo.abaenglsh.challenge.di.scope.Activity;
import com.paulo.abaenglsh.challenge.view.detail.TvShowDetailActivity;

import dagger.Component;

/**
 * Created by pcamilo on 26/07/2019
 */
@Activity
@Component(dependencies = AppComponent.class, modules = TvShowDetailModule.class)
public interface DetailComponent {
    void inject(TvShowDetailActivity activity);
}
