package com.paulo.abaenglsh.challenge.view.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.paulo.abaenglsh.challenge.AbaChallengeApplication;
import com.paulo.abaenglsh.challenge.R;
import com.paulo.abaenglsh.challenge.databinding.ActivityMainBinding;
import com.paulo.abaenglsh.challenge.di.modules.DaggerMainComponent;
import com.paulo.abaenglsh.challenge.di.modules.MainModule;
import com.paulo.abaenglsh.challenge.model.TvShowModel;
import com.paulo.abaenglsh.challenge.utils.Utils;
import com.paulo.abaenglsh.challenge.view.BaseActivity;
import com.paulo.abaenglsh.challenge.view.detail.TvShowDetailActivity;
import com.paulo.abaenglsh.challenge.viewmodel.tvshow.TvShowViewModel;

import java.util.List;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements TvShowResultAdapter.OnItemClickListener {

    @Inject
    TvShowViewModel mMainViewModel;
    private TvShowResultAdapter mTvShowResultAdapter;
    private boolean isFetchingMovies;

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initInjectors() {
        DaggerMainComponent.builder()
                .appComponent(getAbaApplication().getAppComponent())
                .mainModule(new MainModule())
                .build().inject(this);
    }

    @Override
    protected void initBinding() {
        getBinding().setViewModel(mMainViewModel);

        if (Utils.isNetworkConnected(this))
            getPopularTvShows(true);
        else
            showError(AbaChallengeApplication.getInstance().getResources().getString(R.string.warning_no_internet_connection));

        getBinding().tvRetry.setOnClickListener(view -> retryCallService());

        setTitle(getString(R.string.app_title_main));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_refresh) {
            mTvShowResultAdapter = null;
            mMainViewModel.newRequest();
            retryCallService();
            return true;
        }
        return true;
    }

    /**
     * Get all popular tv shows from service
     *
     * @param isLoading this variable indicate if show or not loading
     */
    private void getPopularTvShows(boolean isLoading) {
        getBinding().rvTvShows.setVisibility(View.VISIBLE);
        getBinding().contentTvShows.setVisibility(View.GONE);

        if (isLoading)
            mMainViewModel.showLoading();
        else
            mMainViewModel.hideLoading();

        mMainViewModel.getPopularTvShows().observe(this, res -> {
            if (res != null && res.isSuccess()) {
                updateTvShows(res.getData());
            } else {
                showError(getResources().getString(R.string.warning_server_error));
            }
        });

        isFetchingMovies = false;

        if (mTvShowResultAdapter != null)
            mTvShowResultAdapter.showLoading(false);
    }

    /**
     * Retry the service again when is necessary
     */
    private void retryCallService() {
        if (Utils.isNetworkConnected(this))
            getPopularTvShows(true);
        else {
            Toast.makeText(this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Update the recycle view with all data retrieved
     *
     * @param result the list of popular tv shows
     */
    private void updateTvShows(List<TvShowModel> result) {
        if (mTvShowResultAdapter == null) {
            mTvShowResultAdapter = new TvShowResultAdapter(this);
            mTvShowResultAdapter.submitList(result);

            getBinding().rvTvShows.setAdapter(mTvShowResultAdapter);

            if (getBinding().rvTvShows.getItemDecorationCount() > 0)
                getBinding().rvTvShows.removeItemDecorationAt(0);

            LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            getBinding().rvTvShows.setLayoutManager(layoutManager);
            getBinding().rvTvShows.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
            setupOnScrollListener();
        } else {
            mTvShowResultAdapter.submitList(result);
        }
    }

    /**
     * Set up the scroll listener of RecycleView
     */
    private void setupOnScrollListener() {
        final LinearLayoutManager manager = new LinearLayoutManager(this);
        getBinding().rvTvShows.setLayoutManager(manager);
        getBinding().rvTvShows.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (recyclerView.getAdapter() != null && manager.findLastCompletelyVisibleItemPosition() == recyclerView.getAdapter().getItemCount() - 1) {
                    if (!isFetchingMovies) {
                        if (mMainViewModel.pageTvShows < mMainViewModel.totalPagesTvShows) {
                            if (Utils.isNetworkConnected(getApplication())) {
                                isFetchingMovies = true;
                                mMainViewModel.pageTvShows++;
                                mTvShowResultAdapter.showLoading(true);
                                new Handler().postDelayed(() -> getPopularTvShows(false), 2000);
                            } else {
                                showError(getString(R.string.warning_no_internet_connection));
                            }
                        }
                    }
                }
            }
        });
    }

    /**
     * Show an error for internet or get data from service
     *
     * @param message the message error that will be displayed
     */
    private void showError(String message) {
        mMainViewModel.showLoading.set(View.GONE);

        getBinding().contentTvShows.setVisibility(View.VISIBLE);
        getBinding().rvTvShows.setVisibility(View.GONE);
        getBinding().tvError.setText(message);
    }

    @SuppressLint("NewApi")
    @Override
    public void onItemClick(TvShowModel model, TextView title, ImageView imageView) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TvShowModel.TV_SHOW_MODEL, model);

        Intent intent = new Intent(this, TvShowDetailActivity.class);
        intent.putExtras(bundle);

        TransitionSet set = new TransitionSet();
        set.addTransition(new Fade());

        // set an exit and enter transition
        getWindow().setEnterTransition(new Explode());
        getWindow().setExitTransition(new Explode());

        Pair<View, String> p1 = Pair.create(imageView, getString(R.string.transaction_img_movie));
        Pair<View, String> p2 = Pair.create(title, getString(R.string.transaction_title_movie));

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1, p2);

        startActivity(intent, options.toBundle());
    }
}
