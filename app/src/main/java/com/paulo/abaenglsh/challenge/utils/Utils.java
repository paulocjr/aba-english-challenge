package com.paulo.abaenglsh.challenge.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by pcamilo on 26/07/2019
 */
public class Utils {

    /**
     * Check if has network connected
     *
     * @return true or false if has network access
     */
    public static boolean isNetworkConnected(Context context) {

        boolean isNetwork = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        // connected to the internet
        if (activeNetwork != null) switch (activeNetwork.getType()) {
            case ConnectivityManager.TYPE_WIFI:
                // connected to wifi
                isNetwork = true;
                break;
            case ConnectivityManager.TYPE_MOBILE:
                // connected to mobile data
                isNetwork = true;
                break;
        }

        return isNetwork;
    }

}
