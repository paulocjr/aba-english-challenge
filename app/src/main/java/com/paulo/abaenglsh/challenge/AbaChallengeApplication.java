package com.paulo.abaenglsh.challenge;

import android.app.Application;

import androidx.appcompat.app.AppCompatDelegate;

import com.paulo.abaenglsh.challenge.di.app.AppComponent;
import com.paulo.abaenglsh.challenge.di.app.AppModule;
import com.paulo.abaenglsh.challenge.di.app.DaggerAppComponent;
import com.paulo.abaenglsh.challenge.service.APIClient;

/**
 * Created by pcamilo on 26/07/2019
 */
public class AbaChallengeApplication extends Application {

    private APIClient mApiClient;
    private static AbaChallengeApplication mInstance;
    private AppComponent mAppComponent;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        //Here you pass the URL from services for using the API.
        mApiClient = new APIClient(this, BuildConfig.APP_BASE_URL);

        //This configuration is important to use Dependency Injection (AppModule/AppComponent)
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public APIClient getApiClient() {
        return mApiClient;
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }

    public synchronized static AbaChallengeApplication getInstance() {
        return mInstance;
    }
}
