package com.paulo.abaenglsh.challenge.di.modules;

import com.paulo.abaenglsh.challenge.di.app.AppComponent;
import com.paulo.abaenglsh.challenge.di.scope.Activity;
import com.paulo.abaenglsh.challenge.view.main.MainActivity;

import dagger.Component;

/**
 * Created by pcamilo on 26/07/2019
 */
@Activity
@Component(dependencies = AppComponent.class, modules = MainModule.class)
public interface MainComponent {
    void inject(MainActivity activity);
}
