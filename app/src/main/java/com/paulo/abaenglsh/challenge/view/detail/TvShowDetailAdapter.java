package com.paulo.abaenglsh.challenge.view.detail;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;

import com.paulo.abaenglsh.challenge.R;
import com.paulo.abaenglsh.challenge.databinding.CarouselItemTvShowBinding;
import com.paulo.abaenglsh.challenge.model.TvShowModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by pcamilo on 07/29/19.
 */
public class TvShowDetailAdapter extends PagerAdapter {

    private List<TvShowModel> mTvShowList;
    private CarouselItemTvShowBinding bind;
    private OnItemClickListener onItemClickListener;

    public OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public TvShowDetailAdapter(List<TvShowModel> list) {
        this.mTvShowList = list;
    }

    @Override
    public int getCount() {
        return mTvShowList != null ? mTvShowList.size() : 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.carousel_item_tv_show, container, false);
        container.addView(view);
        bind = DataBindingUtil.bind(view);
        bindView(position);
        return view;
    }

    private void bindView(int position) {
        bind.clickableView.setOnClickListener(v -> {
            if (onItemClickListener != null)
                onItemClickListener.onItemClick(mTvShowList.get(position));
        });

        if (mTvShowList.get(position).getPosterPath() != null) {
            Picasso.get()
                    .load(mTvShowList.get(position).getPosterPath())
                    .into(bind.ivTvShow);
        }

        bind.tvTvShowName.setText(mTvShowList.get(position).getName());
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public interface OnItemClickListener {
        void onItemClick(TvShowModel model);
    }

    public void updateData(List<TvShowModel> data) {
        mTvShowList = data;
        notifyDataSetChanged();
    }

}
