package com.paulo.abaenglsh.challenge.view;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.paulo.abaenglsh.challenge.AbaChallengeApplication;

/**
 * Created by pcamilo on 26/07/2019
 */
public abstract class BaseActivity<T extends ViewDataBinding> extends AppCompatActivity {

    T mViewDataBinding;

    /**
     * This method is used to set up layout view on Activity or Fragment
     *
     * @return int the resourceId
     */
    protected abstract int getContentLayoutId();

    /**
     * Init some injectors for dagger components
     */
    protected abstract void initInjectors();

    /**
     * Init the binding for layouts on Activity or Fragment
     */
    protected abstract void initBinding();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewDataBinding = DataBindingUtil.setContentView(this, getContentLayoutId());
        initInjectors();
        initBinding();
    }

    /**
     * Returns the current binding of layout
     *
     * @return the T is a generic type
     */
    protected T getBinding() {
        return mViewDataBinding;
    }

    /**
     * Returns the application
     *
     * @return the application
     */
    public AbaChallengeApplication getAbaApplication() {
        return ((AbaChallengeApplication) getApplication());
    }
}
