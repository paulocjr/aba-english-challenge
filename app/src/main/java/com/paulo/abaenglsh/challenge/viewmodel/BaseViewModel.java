package com.paulo.abaenglsh.challenge.viewmodel;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.ViewModel;

/**
 * Created by pcamilo on 26/07/2019
 */
public abstract class BaseViewModel extends ViewModel implements LifecycleOwner {

    /**
     * This variable is used to show or hide loading in Views using Observable field.
     */
    public ObservableField<Integer> showLoading = new ObservableField<>();

    private LifecycleRegistry mLifecycleRegistry = new LifecycleRegistry(this);

    protected BaseViewModel() {
        startListening();
    }

    /**
     * Stopping the life cycle of application
     */
    public void stopListening() {
        mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP);
    }

    /**
     * Starting the life cycle of application
     */
    public void startListening() {
        mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START);
    }

    /**
     * Show loading in layouts
     */
    public void showLoading() {
        this.showLoading.set(View.VISIBLE);
    }

    /**
     * Hide loading in layouts
     */
    public void hideLoading() {
        this.showLoading.set(View.GONE);
    }

    /**
     * Returns the life cycle registered
     */
    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return mLifecycleRegistry;
    }
}
