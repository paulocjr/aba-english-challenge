package com.paulo.abaenglsh.challenge.service.models;

import java.io.Serializable;

public class BaseData<T> implements Serializable {

    private String statusMessage;
    private int statusCode;
    private T data;
    private boolean success;

    public BaseData(T data, boolean success) {
        this.data = data;
        this.success = success;
    }

    public BaseData() {

    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public T getData() {
        return data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
