package com.paulo.abaenglsh.challenge.di.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.paulo.abaenglsh.challenge.service.APIClient;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by pcamilo on 26/07/2019
 */
@Singleton
@Component(modules = {AppModule.class, ViewModelBuilder.class})
public interface AppComponent {

    Application application();

    Context context();

    Gson gson();

    APIClient apiClient();

}
