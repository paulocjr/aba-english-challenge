package com.paulo.abaenglsh.challenge.di.app;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.paulo.abaenglsh.challenge.AbaChallengeApplication;
import com.paulo.abaenglsh.challenge.service.APIClient;
import com.paulo.abaenglsh.challenge.viewmodel.ViewModelProviderFactory;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by pcamilo on 26/07/2019
 */
@Module
public class AppModule {

    Application app;

    public AppModule(Application app) {
        this.app = app;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return app;
    }

    @Provides
    @Singleton
    APIClient provideApiClient(Application app) {
        return ((AbaChallengeApplication) app).getApiClient();
    }

    @Provides
    @Singleton
    Picasso providePicasso(APIClient apiClient) {
        return apiClient.getPicasso();
    }

    @Provides
    @Singleton
    Context provideContext() {
        return app.getApplicationContext();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new Gson();
    }
}
