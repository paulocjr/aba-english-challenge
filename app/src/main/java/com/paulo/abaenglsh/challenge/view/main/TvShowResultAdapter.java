package com.paulo.abaenglsh.challenge.view.main;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.paulo.abaenglsh.challenge.R;
import com.paulo.abaenglsh.challenge.databinding.LayoutItemTvshowBinding;
import com.paulo.abaenglsh.challenge.model.TvShowModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TvShowResultAdapter extends RecyclerView.Adapter<TvShowResultAdapter.TvShowViewHolder> {

    private OnItemClickListener onItemClickListener;
    private List<TvShowModel> mItems = new ArrayList<>();
    private boolean loading = false;

    TvShowResultAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public TvShowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_tvshow, parent, false);
        LayoutItemTvshowBinding bind = DataBindingUtil.bind(view);
        return new TvShowViewHolder(Objects.requireNonNull(bind));
    }

    @Override
    public void onBindViewHolder(@NonNull TvShowViewHolder holder, int position) {
        TvShowModel model = mItems.get(position);

        holder.mBinding.tvTitle.setText(model.getName());
        holder.mBinding.tvDate.setText(model.getFirstAirDate());
        holder.mBinding.tvRatingValue.setText(String.valueOf(model.getVoteAverage()));

        if (position == getItemCount() - 1 && loading)
            holder.mBinding.loading.setVisibility(View.VISIBLE);
        else
            holder.mBinding.loading.setVisibility(View.GONE);

        setupImageView(model, holder);
    }

    /**
     * Set an Image on ImageView item of list
     * @param model
     * @param holder
     */
    private void setupImageView(TvShowModel model, TvShowViewHolder holder) {
        if (model.getPosterPath() != null) {
            Picasso.get()
                    .load(model.getPosterPath())
                    .into(holder.mBinding.imgTvShow);
        }
    }

    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    /**
     * Set all data in layout item
     *
     * @param data the data with elements
     */
    public void submitList(List<TvShowModel> data) {
        this.mItems = data;
        notifyDataSetChanged();
    }

    //ViewHolder for layout_item_tvshow
    public class TvShowViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        LayoutItemTvshowBinding mBinding;

        TvShowViewHolder(@NonNull LayoutItemTvshowBinding binding) {
            super(binding.getRoot());
            this.mBinding = DataBindingUtil.bind(itemView);
            this.itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(mItems.get(getAdapterPosition()), mBinding.tvTitle, mBinding.imgTvShow);
        }
    }

    public void showLoading(boolean loading) {
        this.loading = loading;
        notifyDataSetChanged();
    }

    /**
     * Interface to get onClickListener on Adapter
     */
    interface OnItemClickListener {
        void onItemClick(final TvShowModel model, TextView title, ImageView imageView);
    }
}
