package com.paulo.abaenglsh.challenge.repository;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.paulo.abaenglsh.challenge.service.APIClient;
import com.paulo.abaenglsh.challenge.service.TVShowService;
import com.paulo.abaenglsh.challenge.service.models.BaseResponse;
import com.paulo.abaenglsh.challenge.service.models.TvShow;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TVShowRepository {

    private static final String TAG = TVShowRepository.class.getSimpleName();
    private TVShowService mTVShowService;

    @Inject
    TVShowRepository(APIClient apiClient){
        this.mTVShowService = apiClient.getRetrofit().create(TVShowService.class);
    }

    /**
     * Returns the popular TV Shows
     *
     * @param page the page what you want to request
     * @param language the language what you want to see the shows
     * @return the list of similar TV Shows
     */
    public LiveData<BaseResponse<List<TvShow>>> getPopularTvShows(int page, String language) {
        MutableLiveData<BaseResponse<List<TvShow>>> data = new MutableLiveData<>();

        this.mTVShowService.getPopularTvShows(page, language).enqueue(new Callback<BaseResponse<List<TvShow>>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<List<TvShow>>> call, @NonNull Response<BaseResponse<List<TvShow>>> res) {
                if (res.isSuccessful() && res.body() != null) {
                    if (res.body().getResults() != null) {
                        data.setValue(new BaseResponse<>(res.body().getResults(), res.body().getTotalResults(), res.body().getTotalPages(), true));
                    } else {
                        data.setValue(new BaseResponse<>(null, res.body().getStatusMessage(), res.body().getStatusCode(), false));
                    }

                } else {
                    data.setValue(new BaseResponse<>(null, false));
                }

            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<List<TvShow>>> call, @NonNull Throwable t) {
                data.setValue(new BaseResponse<>(null, false));
            }
        });

        return data;
    }

    /**
     * Returns the TV show details by identifier
     *
     * @param tvShowId the identifier of TV show
     * @return the TV show specific by identifier
     */
    public LiveData<BaseResponse<TvShow>> getTvShowDetails(int tvShowId) {
        MutableLiveData<BaseResponse<TvShow>> data = new MutableLiveData<>();

        this.mTVShowService.getTvShowDetails(tvShowId).enqueue(new Callback<TvShow>() {
            @Override
            public void onResponse(@NonNull Call<TvShow> call, @NonNull Response<TvShow> res) {
                if (res.isSuccessful() && res.body() != null) {
                    data.setValue(new BaseResponse<>(res.body(),  true));
                } else {
                    data.setValue(new BaseResponse<>(null, false));
                }
            }

            @Override
            public void onFailure(@NonNull Call<TvShow> call, @NonNull Throwable t) {
                data.setValue(new BaseResponse<>(null, false));
            }
        });

        return data;
    }

    /**
     * Returns the similar TV Shows
     *
     * @param tvShowId the identifier of TV show
     * @return the list similar TV Shows
     */
    public LiveData<BaseResponse<List<TvShow>>> getTvShowsSimilar(int tvShowId) {
        MutableLiveData<BaseResponse<List<TvShow>>> data = new MutableLiveData<>();

        this.mTVShowService.getTvShowsSimilar(tvShowId).enqueue(new Callback<BaseResponse<List<TvShow>>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<List<TvShow>>> call, @NonNull Response<BaseResponse<List<TvShow>>> res) {
                if (res.isSuccessful() && res.body() != null) {
                    if (res.body().getResults() != null) {
                        data.setValue(new BaseResponse<>(res.body().getResults(), res.body().getTotalResults(), res.body().getTotalPages(), true));
                    } else {
                        data.setValue(new BaseResponse<>(null, res.body().getStatusMessage(), res.body().getStatusCode(), false));
                    }

                } else {
                    data.setValue(new BaseResponse<>(null, false));
                }

            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<List<TvShow>>> call, @NonNull Throwable t) {
                data.setValue(new BaseResponse<>(null, false));
            }
        });

        return data;
    }
}
