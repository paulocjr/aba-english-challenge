package com.paulo.abaenglsh.challenge.service;

import com.paulo.abaenglsh.challenge.service.models.BaseResponse;
import com.paulo.abaenglsh.challenge.service.models.TvShow;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * TV show service is used to get information on The Movie Database API.
 *
 * Created by pcamilo on 27/07/2019
 */
public interface TVShowService {

    /**
     * Retrieve the popular TV shows
     *
     * @param page the page what you want to request
     * @param language the language what you want to see the shows
     * @return the list with popular TV shows
     */
    @GET("tv/popular")
    Call<BaseResponse<List<TvShow>>> getPopularTvShows(@Query("page") int page, @Query("language") String language);

    /**
     * Returns the TV show details by identifier
     *
     * @param tvShowId the identifier of TV show
     * @return the TV show specific by identifier
     */
    @GET("tv/{tv_id}")
    Call<TvShow> getTvShowDetails(@Path("tv_id") int tvShowId);

    /**
     * Returns the similar TV Shows
     *
     * @param tvShowId the identifier of TV show
     * @return the list similar TV Shows
     */
    @GET("tv/{tv_id}/similar")
    Call<BaseResponse<List<TvShow>>> getTvShowsSimilar(@Path("tv_id") int tvShowId);

}
