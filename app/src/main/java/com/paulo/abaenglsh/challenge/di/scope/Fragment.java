package com.paulo.abaenglsh.challenge.di.scope;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by pcamilo on 26/07/2019
 */
@Scope
@Retention(RUNTIME)
public @interface Fragment {
}
