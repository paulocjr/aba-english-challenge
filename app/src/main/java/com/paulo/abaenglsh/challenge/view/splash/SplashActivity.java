package com.paulo.abaenglsh.challenge.view.splash;

import android.content.Intent;

import com.paulo.abaenglsh.challenge.R;
import com.paulo.abaenglsh.challenge.databinding.ActivitySplashBinding;
import com.paulo.abaenglsh.challenge.view.BaseActivity;
import com.paulo.abaenglsh.challenge.view.main.MainActivity;

import android.os.Handler;

public class SplashActivity extends BaseActivity<ActivitySplashBinding> {

    static int SPLASH_DURATION = 1500;

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initInjectors() {

    }

    @Override
    protected void initBinding() {
        handlerMainActivity();
    }

    /**
     * This method starting the Main Activity in some seconds.
     */
    private void handlerMainActivity() {
        new Handler().postDelayed(() -> {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            overridePendingTransition(R.anim.anim_slide_left, R.anim.anim_slide_left);
            finish();
        }, SPLASH_DURATION);
    }
}
