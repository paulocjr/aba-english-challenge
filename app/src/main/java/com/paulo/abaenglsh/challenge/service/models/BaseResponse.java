package com.paulo.abaenglsh.challenge.service.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseResponse<T> implements Serializable {

    @SerializedName("status_message")
    private String statusMessage;

    @SerializedName("status_code")
    private int statusCode;

    @SerializedName("results")
    private T results;

    @SerializedName("total_results")
    private int totalResults;

    @SerializedName("total_pages")
    private int totalPages;

    private boolean success;

    public BaseResponse(T results, int totalResults, int totalPages, boolean success) {
        this.results = results;
        this.totalResults = totalResults;
        this.totalPages = totalPages;
        this.success = success;
    }

    public BaseResponse(T results, String statusMessage, int statusCode, boolean success) {
        this.results = results;
        this.statusMessage = statusMessage;
        this.statusCode = statusCode;
        this.success = success;
    }

    public BaseResponse(T results,  boolean success) {
        this.results = results;
        this.success = success;
    }

    public BaseResponse() {
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public T getResults() {
        return results;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public boolean isSuccess() {
        return success;
    }
}
