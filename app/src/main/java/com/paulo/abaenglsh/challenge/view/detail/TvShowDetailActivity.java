package com.paulo.abaenglsh.challenge.view.detail;

import android.annotation.SuppressLint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.TransitionInflater;
import android.transition.TransitionSet;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;

import com.paulo.abaenglsh.challenge.R;
import com.paulo.abaenglsh.challenge.databinding.ActivityTvShowDetailActivtyBinding;
import com.paulo.abaenglsh.challenge.di.modules.DaggerDetailComponent;
import com.paulo.abaenglsh.challenge.di.modules.TvShowDetailModule;
import com.paulo.abaenglsh.challenge.model.TvShowModel;
import com.paulo.abaenglsh.challenge.view.BaseActivity;
import com.paulo.abaenglsh.challenge.viewmodel.tvshow.TvShowViewModel;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

public class TvShowDetailActivity extends BaseActivity<ActivityTvShowDetailActivtyBinding> implements TvShowDetailAdapter.OnItemClickListener {

    @Inject
    TvShowViewModel mMainViewModel;
    public static final String TV_SHOW_MODEL = "tv_show_model";
    private TvShowModel mTvShowModel = new TvShowModel();
    private TvShowDetailAdapter mTvShowDetailAdapter;

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_tv_show_detail_activty;
    }

    @Override
    protected void initInjectors() {
        DaggerDetailComponent.builder()
                .appComponent(getAbaApplication().getAppComponent())
                .tvShowDetailModule(new TvShowDetailModule())
                .build().inject(this);
    }

    @Override
    protected void initBinding() {
        receiveDataFromBundle();
    }

    private void receiveDataFromBundle() {
        Bundle bundle = getIntent().getExtras();

        if (bundle == null || !bundle.containsKey(TV_SHOW_MODEL)) {
            setErrorLayout();
        } else {
            mTvShowModel = bundle.getParcelable(TV_SHOW_MODEL);
            if (mTvShowModel != null) {
                getBinding().tbTvShowDetail.setTitle(mTvShowModel.getName());
                setupToolbar();

                if (mTvShowModel.getBackdropPath() != null) {
                    makeImageToolbarTransition(mTvShowModel);
                } else {
                    isNotExpendedAppBar();
                }

                getTvShowDetail(mTvShowModel.getId()); //Call the service to get all details
            }
        }
    }

    /**
     * Get TV show detail by identifier
     *
     * @param id the identifier of TV Shwo
     */
    private void getTvShowDetail(int id) {
        mMainViewModel.getTvShowDetails(id).observe(this, tvShowModel -> {
            if (tvShowModel != null && tvShowModel.getData() != null) {
                setTvShowDetails(tvShowModel.getData());
            } else {
                setErrorLayout();
            }
        });
    }

    /**
     * Execute animation again when the service retrieve some result
     *
     * @param model the object with all details
     */
    public void executeAnimation(TvShowModel model) {
        if (model != null) {
            getBinding().tbTvShowDetail.setTitle(model.getName());
            setupToolbar();

            if (model.getPosterPath() != null) {
                makeImageToolbarTransition(model);
            } else {
                isNotExpendedAppBar();
            }
        }
    }

    /**
     * Populate de viw with data retrieved of Service
     *
     * @param model the TvShow with details
     */
    private void setTvShowDetails(TvShowModel model) {
        getBinding().setModel(model);
        executeAnimation(model);

        getBinding().tbTvShowDetail.invalidate();
        getBinding().tbTvShowDetail.removeAllViews(); //remove the last title and update with new title

        getBinding().tbTvShowDetail.setTitle(model.getName());

        setupToolbar();
        getBinding().appBarLayout.setExpanded(true, true);
        setupTransitionAnimation();

        //All get the similar TV Show :)
        getSimilarTvShows(model.getId());
    }

    /**
     * Get the similar TV Shows
     *
     * @param id the identifier of tv show simialar
     */
    private void getSimilarTvShows(int id) {
        mMainViewModel.getSimilarTvShows(id).observe(this, tvShowModel -> {
            if (tvShowModel != null && tvShowModel.getData() != null && tvShowModel.getData().size() > 0) {
                carouselTvShowSimilar(tvShowModel.getData());
            } else {
                hideTitleSimilar();
            }
        });
    }

    /**
     * Populate the view page if similar tv shows
     *
     * @param data the list of similar TV Show
     */
    private void carouselTvShowSimilar(List<TvShowModel> data) {
        if (mTvShowDetailAdapter != null) {
            mTvShowDetailAdapter.updateData(data);
            getBinding().vpSimilar.setCurrentItem(0);
            return;
        }

        mTvShowDetailAdapter = new TvShowDetailAdapter(data);
        mTvShowDetailAdapter.setOnItemClickListener(this);
        getBinding().vpSimilar.setAdapter(mTvShowDetailAdapter);
    }

    /**
     * Hide title similar TV Shows
     */
    private void hideTitleSimilar() {
        getBinding().tvSimilar.setVisibility(View.GONE);
    }

    /**
     * Show the layout error from service
     */
    private void setErrorLayout() {
        getBinding().appBarLayout.setVisibility(View.GONE);
        getBinding().contentTvShows.setVisibility(View.VISIBLE);
    }

    /**
     * Expend or not the toolbar app when has a image of TvShow
     */
    private void isNotExpendedAppBar() {
        getBinding().bg.scrollTo(0, 0);
        getBinding().appBarLayout.setExpanded(false, false);
    }

    /**
     * Make animation when I receive the image of TvShow or Bundle param
     *
     * @param tvShowModel the tv show parameter
     */
    public void makeImageToolbarTransition(TvShowModel tvShowModel) {
        setupImageToolbar(tvShowModel.getBackdropPath());
        setupTransitionAnimation();
    }

    @SuppressLint("NewApi")
    private void setupTransitionAnimation() {
        TransitionSet setEnter = new TransitionSet();
        setEnter.setDuration(200);
        setEnter.addTransition(TransitionInflater.from(this).inflateTransition(R.transition.text_transaction));
        setEnter.addTransition(new ChangeImageTransform().addTarget(getBinding().ivTvShowImage));
        setEnter.addTransition(new ChangeBounds().addTarget(getBinding().ivTvShowImage));

        TransitionSet setExit = new TransitionSet();
        setExit.setDuration(200);
        setExit.addTransition(TransitionInflater.from(this).inflateTransition(R.transition.text_transaction));
        setEnter.addTransition(new ChangeImageTransform().addTarget(getBinding().ivTvShowImage));
        setEnter.addTransition(new ChangeBounds().addTarget(getBinding().ivTvShowImage));

        getWindow().setSharedElementEnterTransition(setEnter);
        getWindow().setSharedElementReturnTransition(setExit);
    }

    /**
     * Set the toolbar on activity
     *
     * @param imagePath the image that will be displayed on AppBar
     */
    public void setupImageToolbar(String imagePath) {
        Picasso.get()
                .load(imagePath)
                .into(getBinding().ivTvShowImage);
        setupTransitionAnimation();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Respond to the action bar's Up/Home button
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Setup toolbar to activity with data bing
     */
    private void setupToolbar() {
        setSupportActionBar(getBinding().tbTvShowDetail);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp).mutate();
        upArrow.setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(upArrow);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        getBinding().collapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.colorWhite));
        getBinding().collapsingToolbar.setCollapsedTitleTextColor(getResources().getColor(R.color.colorWhite));
    }

    @Override
    public void onItemClick(TvShowModel model) {
        if (model != null) {
            getTvShowDetail(model.getId());
        }
    }
}
