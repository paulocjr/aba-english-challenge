package com.paulo.abaenglsh.challenge.viewmodel.tvshow;

import android.annotation.SuppressLint;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.paulo.abaenglsh.challenge.BuildConfig;
import com.paulo.abaenglsh.challenge.model.TvShowModel;
import com.paulo.abaenglsh.challenge.repository.TVShowRepository;
import com.paulo.abaenglsh.challenge.service.models.BaseData;
import com.paulo.abaenglsh.challenge.service.models.TvShow;
import com.paulo.abaenglsh.challenge.viewmodel.BaseViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by pcamilo on 26/07/2019
 */
public class TvShowViewModel extends BaseViewModel {

    private TVShowRepository tvShowRepository;
    public int pageTvShows = 1;
    public int totalPagesTvShows = 1;
    private List<TvShowModel> tvShowModelList = new ArrayList<>();
    private BaseData<List<TvShowModel>> baseData = new BaseData<>();
    private BaseData<TvShowModel> tvShowData = new BaseData<>();

    @Inject
    public TvShowViewModel(TVShowRepository tvShowRepository) {
        this.tvShowRepository = tvShowRepository;
    }

    /**
     * Get the popular TV Show from Service
     *
     * @return the list of popular tv shows
     */
    @SuppressLint("NewApi")
    public LiveData<BaseData<List<TvShowModel>>> getPopularTvShows() {
        return Transformations.map(tvShowRepository.getPopularTvShows(pageTvShows, Locale.getDefault().toLanguageTag()), res -> {
            if (res.isSuccess() && res.getResults() != null) {
                totalPagesTvShows = res.getTotalPages();

                tvShowModelList.addAll(transformTvShowList(res.getResults()));
                baseData.setSuccess(res.isSuccess());
                baseData.setData(tvShowModelList);
            } else {
                baseData.setData(new ArrayList<>());
            }

            hideLoading();

            return baseData;
        });
    }

    /**
     * Get the TV Show details pass the identifier
     *
     * @param id the identifier
     * @return the tv show specific
     */
    public LiveData<BaseData<TvShowModel>> getTvShowDetails(int id) {
        return Transformations.map(tvShowRepository.getTvShowDetails(id), res -> {
            if (res.isSuccess() && res.getResults() != null) {
                tvShowData.setSuccess(res.isSuccess());
                tvShowData.setData(transformTvShowList(Collections.singletonList(res.getResults())).get(0));
            } else {
                tvShowData.setData(null);
            }

            hideLoading();

            return tvShowData;
        });
    }

    /**
     * Get the similar TV Show from Service
     *
     * @return the list of similar popular tv shows
     *
     */
    //No pass the page because I want to get only 20 items :) be happy!
    public LiveData<BaseData<List<TvShowModel>>> getSimilarTvShows(int tvShowId) {

        BaseData<List<TvShowModel>> data = new BaseData<>();

        return Transformations.map(tvShowRepository.getTvShowsSimilar(tvShowId), res -> {
            if (res.isSuccess() && res.getResults() != null) {
                totalPagesTvShows = res.getTotalPages();
                data.setSuccess(res.isSuccess());
                data.setData(transformTvShowList(res.getResults()));
            } else {
                data.setData(new ArrayList<>());
            }

            hideLoading();

            return data;
        });
    }

    /**
     * Convert the TvShow result from service to TvShowModel for using on View
     * @param data the tv show response
     * @return the list or one object the TvShowModel
     */
    private List<TvShowModel> transformTvShowList(List<TvShow> data){
        List<TvShowModel> result = new ArrayList<>();

        if (data != null) {
            for (TvShow tvShow : data) {
                TvShowModel model = new TvShowModel();
                model.setId(tvShow.getId());
                model.setName(tvShow.getName());
                model.setOriginalName(tvShow.getOriginalName());
                model.setVoteAverage(tvShow.getVoteAverage());
                model.setFirstAirDate(tvShow.getFirstAirDate());
                model.setBackdropPath(tvShow.getBackdropPath());
                model.setOverview(tvShow.getOverview());

                if (tvShow.getPosterPath() != null) { //set image correctly with base url path
                    model.setPosterPath(BuildConfig.BASE_URL_IMG + "w185" + tvShow.getPosterPath()); //Small image
                }

                if (tvShow.getBackdropPath() != null) {
                    model.setBackdropPath(BuildConfig.BASE_URL_IMG + "original"+ tvShow.getPosterPath()); //Big image
                }

                result.add(model);
            }
        }

        return result;
    }

    /**
     * Executing a new request again
     */
    public void newRequest() {
        totalPagesTvShows = 1;
        pageTvShows = 1;
        tvShowModelList = new ArrayList<>();
    }
}
