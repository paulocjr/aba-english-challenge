package com.paulo.abaenglsh.challenge.di.app;

import androidx.lifecycle.ViewModelProvider;

import com.paulo.abaenglsh.challenge.viewmodel.ViewModelProviderFactory;

import dagger.Binds;
import dagger.Module;

/**
 * Created by pcamilo on 27/07/19.
 */

@Module
public abstract class ViewModelBuilder {

    // ViewModel Factory
    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelProviderFactory factory);
}
