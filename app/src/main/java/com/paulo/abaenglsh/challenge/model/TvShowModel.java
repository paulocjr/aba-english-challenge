package com.paulo.abaenglsh.challenge.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.recyclerview.widget.DiffUtil;

import com.paulo.abaenglsh.challenge.BuildConfig;

import java.util.Objects;

public class TvShowModel extends BaseObservable implements Parcelable {

    public static final String TV_SHOW_MODEL = "tv_show_model";

    private int id;
    private String posterPath;
    private String name;
    private String originalName;
    private String overview;
    private int voteCount;
    private float voteAverage;
    private String firstAirDate;
    private String backdropPath;

    protected TvShowModel(Parcel in) {
        id = in.readInt();
        posterPath = in.readString();
        name = in.readString();
        originalName = in.readString();
        overview = in.readString();
        voteCount = in.readInt();
        voteAverage = in.readFloat();
        firstAirDate = in.readString();
        backdropPath = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(posterPath);
        dest.writeString(name);
        dest.writeString(originalName);
        dest.writeString(overview);
        dest.writeInt(voteCount);
        dest.writeFloat(voteAverage);
        dest.writeString(firstAirDate);
        dest.writeString(backdropPath);
    }

    public TvShowModel() {
    }

    public static final Creator<TvShowModel> CREATOR = new Creator<TvShowModel>() {
        @Override
        public TvShowModel createFromParcel(Parcel in) {
            return new TvShowModel(in);
        }

        @Override
        public TvShowModel[] newArray(int size) {
            return new TvShowModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }



    @SuppressLint("NewApi")
    @Override
    public int hashCode() {
        return Objects.hash(id, posterPath, name, originalName, overview, voteCount, voteAverage, firstAirDate, backdropPath);
    }

    public static DiffUtil.ItemCallback<TvShowModel> DIFF_CALLBACK = new DiffUtil.ItemCallback<TvShowModel>() {
        @Override
        public boolean areItemsTheSame(@NonNull TvShowModel oldItem, @NonNull TvShowModel newItem) {
            return oldItem.id == newItem.id;
        }

        @SuppressLint("DiffUtilEquals")
        @Override
        public boolean areContentsTheSame(@NonNull TvShowModel oldItem, @NonNull TvShowModel newItem) {
            return oldItem.equals(newItem);
        }
    };

    @Bindable
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Bindable
    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Bindable
    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    @Bindable
    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    @Bindable
    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    @Bindable
    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    @Bindable
    public float getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getFirstAirDate() {
        return firstAirDate;
    }

    public void setFirstAirDate(String firstAirDate) {
        if (firstAirDate != null) {
            String[] date = firstAirDate.split("-");
            if (date.length > 0) {
                this.firstAirDate = date[0];
            } else{
                this.firstAirDate = "Invalid date.";
            }
        } else {
            this.firstAirDate = "Invalid date.";
        }
    }
}
