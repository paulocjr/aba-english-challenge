package com.paulo.abaenglsh.challenge.di.modules;

import com.paulo.abaenglsh.challenge.di.scope.Activity;
import com.paulo.abaenglsh.challenge.repository.TVShowRepository;
import com.paulo.abaenglsh.challenge.viewmodel.tvshow.TvShowViewModel;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pcamilo on 29/07/2019
 */
@Module
public class TvShowDetailModule {

    public TvShowDetailModule() {
    }

    @Activity
    @Provides
    TvShowViewModel providesTvShowViewModel(TVShowRepository repository) {
        return new TvShowViewModel(repository);
    }
}
