package com.paulo.abaenglsh.challenge.service;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.paulo.abaenglsh.challenge.AbaChallengeApplication;
import com.paulo.abaenglsh.challenge.R;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pcamilo on 26/07/2019
 */
public class APIClient {

    private static APIClient mInstance;

    public synchronized static APIClient getInstance() {
        return mInstance;
    }

    private Retrofit mRetrofit;
    private OkHttpClient mClient;
    private Picasso mPicasso;
    private final String mApiKey = "api_key";

    public APIClient(@NonNull Context context, @NonNull String baseUrl) {
        mInstance = this;
        mClient = new OkHttpClient()
                .newBuilder()
                .addInterceptor(addAPIKeyInterceptor)
                .addInterceptor(getLoggingCapableHttpClient())
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        mRetrofit = new Retrofit
                .Builder()
                .baseUrl(baseUrl)
                .client(mClient)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();

        mPicasso = new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(mClient))
                .build();
    }

    /**
     * Interceptor is used to pass apiKey in all external requests
     */
    private final Interceptor addAPIKeyInterceptor = chain -> {
        Request original = chain.request();
        HttpUrl originalHttpUrl = original.url();

        HttpUrl url = originalHttpUrl.newBuilder()
                .addQueryParameter(mApiKey, AbaChallengeApplication.getInstance().getString(R.string.movie_db_api_key))
                .build();

        Request.Builder requestBuilder = original.newBuilder().url(url);

        Request request = requestBuilder.build();
        return chain.proceed(request);
    };

    /**
     * Interceptor to see all requests and responses on Service
     *
     * @return the log with information about data requested or response
     */
    private HttpLoggingInterceptor getLoggingCapableHttpClient() {
        HttpLoggingInterceptor mLogging = new HttpLoggingInterceptor();
        mLogging.setLevel(HttpLoggingInterceptor.Level.BODY);

        return mLogging;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    public Picasso getPicasso() {
        return mPicasso;
    }

    public OkHttpClient getOkHttpClient() {
        return mClient;
    }

}
